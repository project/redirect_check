CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Troubleshooting

INTRODUCTION
------------
The Redirect check module provides a status check on the destination url.
You can set a fallback url, when the status of the destination url is not 200,
301, 302 or 304.

REQUIREMENTS
------------
This module requires the following modules:
* Redirect (https://drupal.org/project/redirect)

INSTALLATION
------------
 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

 * Enable the Redirect and the Redirect Check module.

CONFIGURATION
-------------
 * Add a redirect and on the bottom you get an extra check field.

TROUBLESHOOTING
---------------
 * The fallback url is only shown when the checkbox is selected.
 * The redirect URL must respond within 10 seconds. After that, the
   fallback URL is used.
